## Texas county boundaries shapefile

Instead of hosting this large file here, providing the URL:

https://gis-txdot.opendata.arcgis.com/datasets/texas-county-boundaries

How to load it into a Google Colab notebook directly:

    !wget https://opendata.arcgis.com/datasets/9b2eb7d232584572ad53bad41c76b04d_0.zip -O counties.zip
    !unzip counties.zip